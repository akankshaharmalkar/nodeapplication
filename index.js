const express=require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app=express();
app.use(cors());
app.use(bodyParser.json())
var port = process.env.PORT || 8002;


app.get('/',(req,res)=>{
    res.send(`Hello World`);
})

app.post('/calculator',(req,res)=>{
    console.log(req.body)
    let rezz = Number(req.body.num1) + Number(req.body.num2) 
    res.render('calculator',{
        // page:'Calculator', 
        // menuId:'Calculator',
        result: rezz
    });
})

const calculator=require('./routes/calculatorRoute');
app.use('/calculatorApplication',calculator);
app.use('/clock',calculator)


app.set('view engine', 'ejs')


app.listen(port);
console.log(`Node Server Is Running On ${port}`);


